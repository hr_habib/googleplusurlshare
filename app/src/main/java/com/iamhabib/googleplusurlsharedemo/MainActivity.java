package com.iamhabib.googleplusurlsharedemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.com.iamhabib.googleplusurlsharedemo.R;
import com.iamhabib.googleplusurlshare.GooglePlusUrlShare;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void share(View v){
        GooglePlusUrlShare.with(MainActivity.this)
                .shareUrl("https://play.google.com/store/apps/developer?id=App+Racks")
                .withText("It's Ok\n")
                .shareNow();
    }
}
