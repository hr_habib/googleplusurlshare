package com.iamhabib.googleplusurlshare;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ShareCompat;
import android.widget.Toast;

import java.io.IOException;

/**
 * Created by HABIB on 4/26/2017.
 */

public class GooglePlusUrlShare {

    public static Builder with(Activity context) {
        return new Builder(context);
    }

    public static class Builder {
        Activity activity;
        private String NO_CONNECTION_MESSAGE = "Sorry! No Internet Connection Found!";
        private String SHARE_URL = "";
        private String SHARE_TEXT = "";
        private String DIALOG_TITLE = "Please Install Google+";
        private String DIALOG_MESSAGE = "If you want to share, you must have to install Google+ app. Are you want to install it?";

        private Builder(Activity activity) {
            this.activity = activity;
        }

        public Builder noConnectionMessage(String message) {
            this.NO_CONNECTION_MESSAGE = message;
            return this;
        }

        public Builder shareUrl(String url) {
            this.SHARE_URL = url;
            return this;
        }

        public Builder withText(String text) {
            this.SHARE_TEXT = text;
            return this;
        }

        public Builder googlePlusNotFoundDialogTitle(String dialogTitle) {
            this.DIALOG_TITLE = dialogTitle;
            return this;
        }

        public Builder googlePlusNotFoundDialogMessage(String dialogMessage) {
            this.DIALOG_MESSAGE = dialogMessage;
            return this;
        }

        public Builder shareNow() {
            if (isGooglePlusInstall(activity)) {
                Intent shareIntent = ShareCompat.IntentBuilder.from(activity)
                        .setText(SHARE_TEXT+"\n"+SHARE_URL)
                        .setType("text/plain")
                        .getIntent()
                        .setPackage("com.google.android.apps.plus");
                activity.startActivity(shareIntent);
                if (!isInternetConnected()) {
                    Toast.makeText(activity, NO_CONNECTION_MESSAGE, Toast.LENGTH_LONG).show();
                }
            } else {
                AlertDialog alertDialog = new AlertDialog.Builder(activity)
                        .setCancelable(false)
                        .setIcon(R.mipmap.ic_launcher)
                        .setTitle(DIALOG_TITLE)
                        .setMessage(DIALOG_MESSAGE)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.apps.plus");
                                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                activity.startActivity(intent);
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .create();

                alertDialog.show();
            }
            return this;


        }

        private static boolean isGooglePlusInstall(Activity context) {
            try {
                context.getPackageManager().getPackageInfo("com.google.android.apps.plus", 0);
                return true;
            } catch (PackageManager.NameNotFoundException e) {
                return false;
            }
        }

        private static boolean isInternetConnected() {
            try {
                return (Runtime.getRuntime().exec("ping -c 1 google.com").waitFor() == 0);
            } catch (InterruptedException e) {
                return false;
            } catch (IOException e) {
                return false;
            }
        }
    }
}
